package com.example.phonebook.servers;

import com.example.phonebook.dto.PhoneNumberDto;
import com.example.phonebook.repository.PhoneNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneNumberServiceImpl implements PhoneNumberService {

    private final PhoneNumberRepository phoneNumberRepository;

    @Autowired
    public PhoneNumberServiceImpl(PhoneNumberRepository phoneNumberRepository) {
        this.phoneNumberRepository = phoneNumberRepository;
    }

    @Override
    public void deletedPhoneNumber(Long id) {

    }

    @Override
    public void updatePhoneNumber(Long id, PhoneNumberDto dto) {

    }

    @Override
    public void addPhoneNumber(PhoneNumberDto dto) {

    }
}
