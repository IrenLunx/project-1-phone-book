package com.example.phonebook.servers;

import com.example.phonebook.dto.UserDto;

public interface UserService {

    void deletedUser(Long id);
    void updateUser(Long id, UserDto dto);
    void addPhoneNumber(UserDto dto);
}
