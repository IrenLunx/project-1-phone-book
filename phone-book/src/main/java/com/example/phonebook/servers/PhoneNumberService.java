package com.example.phonebook.servers;

import com.example.phonebook.dto.PhoneNumberDto;
import com.example.phonebook.model.PhoneNumber;

public interface PhoneNumberService {

    void deletedPhoneNumber(Long id);
    void updatePhoneNumber(Long id, PhoneNumberDto dto);
    void addPhoneNumber(PhoneNumberDto dto);
}
