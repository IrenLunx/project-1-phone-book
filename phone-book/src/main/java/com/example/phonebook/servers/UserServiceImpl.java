package com.example.phonebook.servers;

import com.example.phonebook.dto.UserDto;
import com.example.phonebook.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void deletedUser(Long id) {

    }

    @Override
    public void updateUser(Long id, UserDto dto) {

    }

    @Override
    public void addPhoneNumber(UserDto dto) {

    }
}
