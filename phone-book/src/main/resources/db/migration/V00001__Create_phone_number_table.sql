create sequence hibernate_sequence start 1 increment 1;

create table phone_number
(
    id BIGSERIAL primary key,
    number varchar(20),
    deleted boolean,
    date_created DATE,
    date_deleted DATE,
    id_user BIGINT
);
