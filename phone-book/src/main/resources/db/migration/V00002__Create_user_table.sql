create sequence hibernate_sequence start 1 increment 1;

create table user
(
    id BIGSERIAL primary key,
    first_name varchar(50),
    last_name varchar(50),
    patronymic varchar(50)
);